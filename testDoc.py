from docx import Document

replace_dict = {"密码": "password"}
def check_and_change(document, replace_dict):
    for para in document.paragraphs:
        for i in range(len(para.runs)):
            for key, value in replace_dict.items():
                if key in para.runs[i].text:
                    print(key + "->" + value)
                    para.runs[i].text = para.runs[i].text.replace(key, value)
    return document

old_file = "密码锁设置.docx"
new_file = "New密码锁设置.docx"

def main():
    document = Document(old_file)
    document = check_and_change(document, replace_dict)
    document.save(new_file)

if __name__ == "__main__":
    main()
